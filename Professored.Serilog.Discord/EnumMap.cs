﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Professored.Serilog.Discord
{
    public enum ActivityType
    {
        Playing, Watching, ListeningTo, Streaming, Competing
    }

    public enum BotStatus
    {
        Online, DND, Invisible
    }
}
