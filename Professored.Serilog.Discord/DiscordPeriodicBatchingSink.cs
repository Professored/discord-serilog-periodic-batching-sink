﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace Professored.Serilog.Discord
{
    public class DiscordPeriodicBatchingSink : ILogEventSink
    {
        IFormatProvider _formatProvider;
        DiscordClient _discord;
        ConcurrentQueue<LogEvent> _eventQueue;
        DiscordClientConfig _runningConfig;
        Timer _secondTimer;

        public DiscordPeriodicBatchingSink(DiscordClientConfig config, IFormatProvider provider)
        {
            _runningConfig = config;
            _eventQueue = new ConcurrentQueue<LogEvent>();
            _formatProvider = provider;

            if (_runningConfig.BatchFrequency >= 0)
            {
                _runningConfig.BatchFrequency = 2000;
            }

            if (_runningConfig.MaxLogLength == 0 || _runningConfig.MaxLogLength > 1950)
            {
                _runningConfig.MaxLogLength = 1950;
            }

            _discord = new DiscordClient(new DiscordConfiguration
            {
                Token = config.DiscordToken,
                TokenType = TokenType.Bot,
            });

            var actType = _runningConfig.ActivityType switch
            {
                ActivityType.Playing => DSharpPlus.Entities.ActivityType.Playing,
                ActivityType.Competing => DSharpPlus.Entities.ActivityType.Competing,
                ActivityType.Streaming => DSharpPlus.Entities.ActivityType.Streaming,
                ActivityType.Watching => DSharpPlus.Entities.ActivityType.Watching,
                //"Custom" => ActivityType.Custom, //Not yet supported in DSharpPlus.
                ActivityType.ListeningTo => DSharpPlus.Entities.ActivityType.ListeningTo,
                _ => DSharpPlus.Entities.ActivityType.Playing
            };

            var statusType = _runningConfig.Status switch
            {
                BotStatus.DND => UserStatus.DoNotDisturb,
                BotStatus.Invisible => UserStatus.Invisible,
                BotStatus.Online => UserStatus.Online,
                _ => UserStatus.Online
            };

            DiscordActivity customActivity = new DiscordActivity
            {
                ActivityType = actType,
                Name = _runningConfig.Activity
            };

            _discord.ConnectAsync(customActivity, statusType).GetAwaiter().GetResult();

            _secondTimer = new Timer(GetEmitBundle, null, _runningConfig.BatchFrequency,
                _runningConfig.BatchFrequency);
        }

        public void Emit(LogEvent logEvent)
        {
            _eventQueue.Enqueue(logEvent);
        }

        public void GetEmitBundle(object state)
        {
            StringBuilder outBuilder = new StringBuilder();
            DiscordChannel outputChannel = _discord.GetChannelAsync(_runningConfig.LogChannelId)
                .GetAwaiter().GetResult(); // Async library in a non-async context.

            //Advice says that the StringBuilder MaxCapacity definition
            //can be exceeded, so we do our own checking here.
            do
            {
                if (_eventQueue.IsEmpty)
                {
                    break; //Queue empty, so we abort here.
                }

                if (_eventQueue.TryDequeue(out var evt))
                {
                    var msg = evt.RenderMessage(_formatProvider);
                    var log = $"[{evt.Level}] [{evt.Timestamp}] {msg}";
                    outBuilder.AppendLine(log);
                }
                else
                {
                    break; //Somehow empty despite the check above.
                }
            } while (outBuilder.Length < _runningConfig.MaxLogLength);

            var outString = outBuilder.ToString();

            var blockCodeFormatted = Formatter.BlockCode(outString);

            if (!string.IsNullOrEmpty(outString))
            {
                _discord.SendMessageAsync(outputChannel, blockCodeFormatted).GetAwaiter().GetResult();
            }
        }
        
    }
}
