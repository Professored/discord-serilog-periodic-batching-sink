﻿using Professored.Serilog.Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Professored.Serilog.Discord
{
    public class DiscordClientConfig
    {
        public string DiscordToken { get; set; }
        public ulong LogChannelId { get; set; }
        public string Activity { get; set; }
        public int MaxLogLength { get; set; }
        public int BatchFrequency { get; set; }
        public ActivityType ActivityType { get; set; }
        public BotStatus Status { get; set; }
        
        //public string CustomActivityEmoji { get; set; } //Not yet supported by DS+.
    }
}
