﻿using Serilog;
using Serilog.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Professored.Serilog.Discord
{
    public static class LoggerConfigurationExtension
    {
        public static LoggerConfiguration DiscordPeriodic(
            this LoggerSinkConfiguration loggerConfiguration,
            DiscordClientConfig discordConfiguration,
            IFormatProvider fmtProvider = null)
        {
            return loggerConfiguration.Sink(new DiscordPeriodicBatchingSink(discordConfiguration, fmtProvider));
        }
    }
}
