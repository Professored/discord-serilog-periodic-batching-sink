# README

This logger sink is designed to, on a periodic cycle, send out one message with the logs
for the period up to a maximum length in characters.

## Quickstart
1. Either clone the repository and refer to the project in your solution, or clone 
the repository and build the class library and refer to that in your project.
2. Add a reference to the sink (`using Professored.Serilog.Discord;`).
3. Create a `DiscordClientConfig` object.
```csharp
var config = new DiscordClientConfig 
{
    DiscordToken = "discord_token_string",
    LogChannelId = 123456789012345678 //ulong
}
```
4. Initalize the sink in your logger configuration.
```csharp
Log.Logger = new LoggerConfiguration().WriteTo.DiscordPeriodic(config).CreateLogger();
```

5. Log as per normal.

## Advanced Configuration
The DiscordClientConfig has defaults for most of these values, but they can be overriden by
specifying them.

Every parameter and what values they accept are below:
* `DiscordToken` - the raw string representation of the token to use. Should never be stored in code.
* `LogChannelId` - the ulong channel ID to write logs to.
* `Activity` - what activity the bot should register with. Accepts any string.
* `ActivityType` - what type of activity should the bot be doing? Accepts an `ActivityType`.
* `Status` - what status should the bot display itself as? Accepts a `BotStatus`.
* `MaxLogLength` - what should the 'max length' of rendered log messages be? Values over 1950 are
unsupported, and values over 2000 are rejected by Discord.
* `BatchFrequency` - how often should the bot try and push log messages (in milliseconds)? Settings below 2000
are ignored to remain within Discord rate limits.

## Additional Usage Information
A test application can be found under `Professored.Serilog.Discord.TestApp` - it will write
basic log messages you type into it to demonstrate the sink's behaviour.

To use it, just place a file named `discord.token` with the contents of your Discord bot token in
the same directory as the program, and edit the channel ID it will be using.