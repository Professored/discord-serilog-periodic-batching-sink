﻿using System;
using System.IO;
using Serilog;
namespace Professored.Serilog.Discord.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration().WriteTo.Console()
                .Enrich.FromLogContext()
                .WriteTo.DiscordPeriodic(new DiscordClientConfig
                {
                    Activity = "with the log stream.",
                    BatchFrequency = 2000,
                    DiscordToken = File.ReadAllText("discord.token"),
                    LogChannelId = 1234567890234567894,
                    MaxLogLength = 1800,
                    ActivityType = ActivityType.Playing,
                    Status = BotStatus.Online
                }).CreateLogger();

            Console.WriteLine("Please write any message to write it out to Serilog.");
            do
            {
                Console.Write(">> ");
                var input = Console.ReadLine();
                Console.WriteLine();
                Log.Information(input);

            } while (true);
        }
    }
}
